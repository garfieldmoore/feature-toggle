﻿namespace Toggles.Configuration.Interfaces
{
    public interface ISwitchProviderFactory
    {
        ISwitch Create();
    }
}